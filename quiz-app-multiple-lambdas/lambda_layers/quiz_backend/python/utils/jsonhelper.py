import dataclasses


def convert_to_dictionary(obj):
    '''
    TODO: must verify obj is an instance of dataclass later.
    
    '''
    return dataclasses.asdict(obj)