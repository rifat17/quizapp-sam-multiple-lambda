from db.base import AbstractRepository, AbstractDatabase
from models.quiz import QuizUserResponse
from random import choice

class MockQuizUserResponseRepository(AbstractRepository):
    def __init__(self, db: AbstractDatabase):
        self._db = db

    def get(self, **kwargs):
        '''Get All response from User X for Quiz Y'''
        user_id = kwargs.get('user_id', None)
        quiz_id = kwargs.get('quiz_id', None)
        if not user_id and quiz_id:
            raise KeyError

        res1 = QuizUserResponse(Id=1 ,userId=user_id, quizId=quizId,quizQAId=1, response='response1')
        res2 = QuizUserResponse(Id=2 ,userId=user_id, quizId=quizId,quizQAId=2, response='response2')
        res3 = QuizUserResponse(Id=3 ,userId=user_id, quizId=quizId,quizQAId=3, response='response3')
        quiz_response_list = {
            'user_id' : user_id,
            'quiz_id' : quiz_id,
            'responses' : [res1, res2, res3],
        }
        self._db.select(quiz_response_list)
        return quiz_response_list

    def add(self, **kwargs):
        quiz_user_response: QuizUserResponse = kwargs.get('quiz_user_response', None)
        quiz_user_response.update({'quiz_user_response_id' : choice(list(range(1,1000)))})


        return self._db.insert(quiz_user_response)
