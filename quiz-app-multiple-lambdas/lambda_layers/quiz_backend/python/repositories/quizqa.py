from db.base import AbstractRepository, AbstractDatabase
from models.quiz import QuizQA
from random import choice


class MockQuizQARepository(AbstractRepository):
    def __init__(self, db: AbstractDatabase):
        self._db = db

    def get(self, **kwargs):
        quiz_id = kwargs.get('quiz_id', None)

        qa_list = {
                'quiz_id' : quiz_id,
                'questions' : [
                    {
                        'question_id': 1,
                        'question ': 'Color of sky is?',
                        'opt1' : 'red', 
                        'opt2' : 'green',
                        'opt3' : 'blue',
                        'opt4' : 'black',
                        'ans' : 'blue'
                    },
                    {
                        'question_id': 2,
                        'question ': 'Color of night is?',
                        'opt1' : 'red', 
                        'opt2' : 'green',
                        'opt3' : 'blue',
                        'opt4' : 'black',
                        'ans' : 'black'

                    },
                    {
                        'question_id': 3,
                        'question ': 'Color of moon is?',
                        'opt1' : 'red', 
                        'opt2' : 'white',
                        'opt3' : 'blue',
                        'opt4' : 'black',
                        'ans' : 'white'

                    },
                ]

            }

        return self._db.select(qa_list)

    def add(self, **kwargs):

        quiz_qa = kwargs.get('quiz_qa', None)
        quiz_qa.update({'question_id' : choice(list(range(1,1000)))})

        return self._db.insert(quiz_qa)
