from db.base import AbstractRepository, AbstractDatabase
from models.quiz import Quiz
from random import choice



class MockQuizRepository(AbstractRepository):
    def __init__(self, db: AbstractDatabase):
        self._db = db

    def get(self, **kwargs):
        '''GET QUIZ'''
        quiz_list = [
            {
                'quiz_id' :  1,
                'created_by' : 'user_id_1',
                'quiz_name' : 'quiz_1',
                'quiz_description' : 'a long description of quiz_1',
                'created_at' : 'time1', 
            },
            {
                'quiz_id' : 2,
                'created_by' : 'user_id_2',
                'quiz_name' : 'quiz_2',
                'quiz_description' : 'a long descriptionof quiz_2',
                'created_at' : 'time2', 
            },
        ]
        return self._db.select(quiz_list)


    def add(self, **kwargs):
        quiz = kwargs['quiz']
        
        quiz.update({'quiz_id' : choice(list(range(1,1000)))})
        
        return self._db.insert(quiz)
