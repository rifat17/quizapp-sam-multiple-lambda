import json
from models.quiz import QuizQA
from utils.jsonhelper import convert_to_dictionary

# from ...lambda_layers.quiz_backend.python.models.quiz import QuizQA


# import requests


def lambda_handler(event, context):
    """Sample pure Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """

    # try:
    #     ip = requests.get("http://checkip.amazonaws.com/")
    # except requests.RequestException as e:
    #     # Send some context about this error to Lambda Logs
    #     print(e)

    #     raise e

    quiz_question = QuizQA(
        quiz_id=1,
        question='What is the question?',
        opt1='this is the question.',
        opt2='i dont know.',
        opt3='maybe that is the question.',
        opt4='question is something unknown',
        ans='this is the question.',
        Id='1'
    )
    quiz_question = convert_to_dictionary(quiz_question)

    return {
        "statusCode": 200,
        "body": json.dumps({
            "message": "hello world",
            'quiz_question' : quiz_question,
            # "location": ip.text.replace("\n", "")
        }),
    }
