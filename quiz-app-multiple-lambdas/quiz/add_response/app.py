import json

from models.quiz import QuizUserResponse
from utils.jsonhelper import convert_to_dictionary


# from ...lambda_layers.quiz_backend.python.models.quiz import QuizUserResponse

# import requests


def lambda_handler(event, context):
    """Sample pure Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """

    # try:
    #     ip = requests.get("http://checkip.amazonaws.com/")
    # except requests.RequestException as e:
    #     # Send some context about this error to Lambda Logs
    #     print(e)

    #     raise e

    response = QuizUserResponse(
        userId=1001,
        quizId=1,
        quizQAId=1,
        response='i dont know.',
        Id=1
        
    )

    response = convert_to_dictionary(response)

    return {
        "statusCode": 200,
        "body": json.dumps({
            "message": "hello world",
            "response" : response,
            # "location": ip.text.replace("\n", "")
        }),
    }
