import json

from models.quiz import Quiz
from utils.jsonhelper import convert_to_dictionary


# from ...lambda_layers.quiz_backend.python.models.quiz import Quiz

# import requests


def lambda_handler(event, context):
    """Sample pure Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """

    # try:
    #     ip = requests.get("http://checkip.amazonaws.com/")
    # except requests.RequestException as e:
    #     # Send some context about this error to Lambda Logs
    #     print(e)

    #     raise e

    quiz1 = Quiz(
        created_by=1000, 
        name='SAM Quiz1', 
        description="Test about SAM1", 
        question_count=10,
        Id=1)

    quiz2 = Quiz(
        created_by=1003, 
        name='SAM Quiz2', 
        description="Test about SAM2", 
        question_count=5,
        Id=2)

    quiz1 = convert_to_dictionary(quiz1)
    quiz2 = convert_to_dictionary(quiz2)
    quizes = [quiz1, quiz2]

    return {
        "statusCode": 200,
        "body": json.dumps({
            "message": "hello world",
            "quizes" : quizes,
            # "location": ip.text.replace("\n", "")
        }),
    }
