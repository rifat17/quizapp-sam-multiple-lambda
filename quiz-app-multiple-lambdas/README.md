```

├── quiz-app-multiple-lambdas
│   ├── events
│   │   └── event.json
│   ├── __init__.py
│   ├── lambda_layers
│   │   └── quiz_backend
│   │       └── python
│   │           ├── db
│   │           │   ├── base.py
│   │           │   ├── __init__.py
│   │           │   └── mock_db.py
│   │           ├── models
│   │           │   └── quiz.py
│   │           ├── repositories
│   │           │   ├── __init__.py
│   │           │   ├── quiz.py
│   │           │   ├── quizqa.py
│   │           │   └── response.py
│   │           └── utils
│   │               └── jsonhelper.py
│   ├── quiz
│   │   ├── add_question
│   │   │   ├── app.py
│   │   │   ├── __init__.py
│   │   │   └── requirements.txt
│   │   ├── add_quiz
│   │   │   ├── app.py
│   │   │   ├── __init__.py
│   │   │   └── requirements.txt
│   │   ├── add_response
│   │   │   ├── app.py
│   │   │   ├── __init__.py
│   │   │   └── requirements.txt
│   │   ├── backup
│   │   │   ├── app.py
│   │   │   ├── __init__.py
│   │   │   └── requirements.txt
│   │   ├── get_all_quiz
│   │   │   ├── app.py
│   │   │   ├── __init__.py
│   │   │   └── requirements.txt
│   │   └── get_quiz
│   │       ├── app.py
│   │       ├── __init__.py
│   │       └── requirements.txt
│   ├── README.md
│   ├── samconfig.toml
│   ├── template.yaml
│   └── tests
│       ├── __init__.py
│       ├── integration
│       │   ├── __init__.py
│       │   └── test_api_gateway.py
│       ├── requirements.txt
│       └── unit
│           ├── __init__.py
│           └── test_handler.py
└── README.md

```